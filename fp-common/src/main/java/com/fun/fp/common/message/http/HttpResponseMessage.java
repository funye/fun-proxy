package com.fun.fp.common.message.http;

import lombok.Data;

import java.util.Map;

@Data
public class HttpResponseMessage {

    private String ip;
    private int port;

    private String protocolVersion;
    private int status;
    private String remark;

    private Map<String, String> headers;
    private byte[] body;
}

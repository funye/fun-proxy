package com.fun.fp.common.message.http;

import lombok.Data;

@Data
public class HttpRegisterMessage implements java.io.Serializable {

    private String appHost;
    private int appPort;
    private String domainName;
    private String appId;

}

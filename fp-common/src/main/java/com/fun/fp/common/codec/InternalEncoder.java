package com.fun.fp.common.codec;

import cn.hutool.json.JSONUtil;
import com.fun.fp.common.message.InternalMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.StandardCharsets;

public class InternalEncoder extends MessageToByteEncoder<InternalMessage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, InternalMessage msg, ByteBuf out) throws Exception {

        byte[] data = JSONUtil.toJsonStr(msg).getBytes(StandardCharsets.UTF_8);
        out.writeInt(data.length);
        out.writeBytes(data);
    }
}

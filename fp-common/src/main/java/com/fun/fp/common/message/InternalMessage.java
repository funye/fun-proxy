package com.fun.fp.common.message;

import lombok.Data;

@Data
public class InternalMessage implements java.io.Serializable {

    private int messageType;
    private Object data;
}

package com.fun.fp.common.message;

public enum MessageTypeEnum {
    HTTP_SERVER_REGISTER(1, "http服务端注册"),
    HTTP_REQUEST(2, "http请求"),
    HTTP_RESPONSE(3, "http响应"),
    ;

    private int type;
    private String remark;

    MessageTypeEnum(int type, String remark) {
        this.type = type;
        this.remark = remark;
    }

    public int getType() {
        return type;
    }

    public String getRemark() {
        return remark;
    }

    public static MessageTypeEnum of(int code) {
        for (MessageTypeEnum messageTypeEnum: MessageTypeEnum.values()) {
            if (messageTypeEnum.type==code) {
                return messageTypeEnum;
            }
        }
        return null;
    }
}

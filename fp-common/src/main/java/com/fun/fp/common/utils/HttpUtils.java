package com.fun.fp.common.utils;

import com.fun.fp.common.message.http.HttpRequestMessage;
import com.fun.fp.common.message.http.HttpResponseMessage;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.*;

import java.util.HashMap;
import java.util.Map;

public class HttpUtils {

    public static HttpRequestMessage buildInternalHttpReq(FullHttpRequest request, String host, int port) {

        HttpRequestMessage message = new HttpRequestMessage();
        message.setIp(host);
        message.setPort(port);
        message.setMethod(request.method().name());
        message.setUri(request.uri());
        message.setProtocolVersion(request.protocolVersion().text());
        byte[] bytes = new byte[request.content().readableBytes()];
        request.content().readBytes(bytes);
        message.setBody(bytes);

        Map<String, String> headers = new HashMap<>();
        if (request.headers().size() > 0) {
            for (String header : request.headers().names()) {
                headers.put(header, request.headers().get(header));
            }
        }
        message.setHeaders(headers);

        return message;
    }

    public static HttpResponseMessage buildInternalHttpResp(FullHttpResponse response, String host, int port) {
        HttpResponseMessage message = new HttpResponseMessage();
        message.setIp(host);
        message.setPort(port);
        message.setProtocolVersion(response.protocolVersion().text());
        message.setStatus(response.status().code());
        message.setRemark(response.status().reasonPhrase());
        byte[] bytes = new byte[response.content().readableBytes()];
        response.content().readBytes(bytes);
        message.setBody(bytes);

        Map<String, String> headers = new HashMap<>();
        if (response.headers().size() > 0) {
            for (String header : response.headers().names()) {
                headers.put(header, response.headers().get(header));
            }
        }
        message.setHeaders(headers);
        return message;
    }

    public static FullHttpRequest buildHttpReq(HttpRequestMessage message) {


        HttpMethod method = HttpMethod.valueOf(message.getMethod());
        HttpVersion httpVersion = HttpVersion.valueOf(message.getProtocolVersion());


        FullHttpRequest request = new DefaultFullHttpRequest(httpVersion, method, message.getUri(), Unpooled.copiedBuffer(message.getBody()));
        HttpHeaders headers = request.headers();
        if (message.getHeaders().size() > 0) {
            message.getHeaders().forEach((k,v)-> {
                headers.set(k, v);
            });
        }
        return request;
    }

    public static FullHttpResponse buildHttpResp(HttpResponseMessage message) {


        HttpVersion httpVersion = HttpVersion.valueOf(message.getProtocolVersion());
        HttpResponseStatus status = HttpResponseStatus.valueOf(message.getStatus(), message.getRemark());

        FullHttpResponse response = new DefaultFullHttpResponse(httpVersion, status, Unpooled.copiedBuffer(message.getBody()));
        HttpHeaders headers = response.headers();
        if (message.getHeaders().size() > 0) {
            message.getHeaders().forEach((k,v)-> {
                headers.set(k, v);
            });
        }
        return response;
    }



}

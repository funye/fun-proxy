package com.fun.fp.common.message.http;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class HttpProxyInfo {

    private String upstreamHost;
    private int upstreamPort;
    private String upstreamAppId;
    private String upstreamDomainName;
}

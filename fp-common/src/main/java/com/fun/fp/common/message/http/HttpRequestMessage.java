package com.fun.fp.common.message.http;

import lombok.Data;

import java.util.Map;

@Data
public class HttpRequestMessage {

    private String ip;
    private int port;
    private String method;
    private String uri;
    private String protocolVersion;
    private Map<String, String> headers;
    private byte[] body;

}

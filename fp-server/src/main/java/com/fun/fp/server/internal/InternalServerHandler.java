package com.fun.fp.server.internal;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fun.fp.common.message.InternalMessage;
import com.fun.fp.common.message.MessageTypeEnum;
import com.fun.fp.common.message.http.HttpRegisterMessage;
import com.fun.fp.common.message.http.HttpResponseMessage;
import com.fun.fp.common.utils.HttpUtils;
import com.fun.fp.server.http.HttpChannelHolder;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InternalServerHandler extends SimpleChannelInboundHandler<InternalMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, InternalMessage msg) throws Exception {
        MessageTypeEnum messageTypeEnum = MessageTypeEnum.of(msg.getMessageType());
        if (messageTypeEnum == null) {
            log.error("messageTypeEnum is null");
            return;
        }
        switch (messageTypeEnum) {
            case HTTP_SERVER_REGISTER:
                HttpRegisterMessage registerMessage = JSONUtil.toBean((JSONObject) msg.getData(), HttpRegisterMessage.class);
                Connection conn = Connection.builder()
                        .domainName(registerMessage.getDomainName())
                        .host(registerMessage.getAppHost())
                        .port(registerMessage.getAppPort())
                        .ctx(ctx)
                        .build();
                InternalChannelHolder.addHttpServer(conn);
                break;
            case HTTP_RESPONSE:
                HttpResponseMessage responseMessage = JSONUtil.toBean((JSONObject) msg.getData(), HttpResponseMessage.class);
                Connection clientConn = HttpChannelHolder.get(responseMessage.getIp(), responseMessage.getPort());
                FullHttpResponse response = HttpUtils.buildHttpResp(responseMessage);
                clientConn.getCtx().writeAndFlush(response).addListener(new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture future) throws Exception {
                        HttpChannelHolder.remove(responseMessage.getIp(), responseMessage.getPort());
                        future.channel().close();
                    }
                });
                break;
            default:break;
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("close channel....", cause);
    }


}

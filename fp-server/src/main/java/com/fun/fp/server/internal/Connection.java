package com.fun.fp.server.internal;

import io.netty.channel.ChannelHandlerContext;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Connection {

    private String host;
    private int port;
    private String domainName;
    private ChannelHandlerContext ctx;
}

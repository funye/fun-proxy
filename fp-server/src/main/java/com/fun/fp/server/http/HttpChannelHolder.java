package com.fun.fp.server.http;

import com.fun.fp.server.internal.Connection;

import java.util.concurrent.ConcurrentHashMap;

public class HttpChannelHolder {

    // key=ip:port
    private static ConcurrentHashMap<String, Connection> httpAppServerMap = new ConcurrentHashMap();

    public static void add(Connection conn) {
        httpAppServerMap.put(conn.getHost() + ":" + conn.getPort(), conn);
    }

    public static Connection get(String ip, int port) {
        String key = ip + ":" + port;
        if (!httpAppServerMap.containsKey(key)) {
            return null;
        }
        return httpAppServerMap.get(key);
    }

    public static Connection remove(String ip, int port) {
        String key = ip + ":" + port;
        return httpAppServerMap.remove(key);
    }

}

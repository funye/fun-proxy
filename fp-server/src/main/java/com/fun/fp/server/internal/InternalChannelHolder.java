package com.fun.fp.server.internal;

import com.fun.fp.server.route.HttpRoute;

import java.util.concurrent.ConcurrentHashMap;

public class InternalChannelHolder {

    // key=ip:port
    private static ConcurrentHashMap<String, Connection> httpAppServerMap = new ConcurrentHashMap();

    public static void addHttpServer(Connection conn) {
        httpAppServerMap.put(conn.getHost() + ":" + conn.getPort(), conn);
    }

    public static void removeHttpServer(Connection conn) {
        httpAppServerMap.put(conn.getHost() + ":" + conn.getPort(), conn);
    }

    public static Connection getHttpServer(HttpRoute.Upstream upstream) {
        String key = upstream.getHost() + ":" + upstream.getPort();
        if (!httpAppServerMap.containsKey(key)) {
            return null;
        }
        return httpAppServerMap.get(key);
    }

}

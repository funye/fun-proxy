package com.fun.fp.server;

import com.fun.fp.server.http.HttpServer;
import com.fun.fp.server.internal.InternalServer;
import com.fun.fp.server.route.HttpRoute;
import com.fun.fp.server.route.HttpRouteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class ServerApp {

    private static final Logger log = LoggerFactory.getLogger(ServerApp.class);

    public static void main(String[] args) {

        Properties properties = loadProperties();
        if (properties == null) {
            log.error("start ServerApp , get properties error");
            return;
        }

        try {
            // start http server
            List<HttpRoute> routes = HttpRouteConfig.allRoute();
            for (HttpRoute route : routes) {
                new HttpServer(route.getProxyPort()).start();
            }

            // start internal sever
            new InternalServer(Integer.valueOf(properties.getProperty("proxy.server.port", "9999"))).start();

            log.info("==========ServerApp SUCCESS !!!=============");
        } catch (Exception e) {
            log.error("start ServerApp error", e);
        }
    }

    public static Properties loadProperties() {
        try {
            Properties properties = new Properties();
            // 使用ClassLoader加载properties配置文件生成对应的输入流
            InputStream in = ServerApp.class.getClassLoader().getResourceAsStream("config.properties");
            // 使用properties对象加载输入流
            properties.load(in);

            return properties;
        } catch (Exception e) {
            log.error("get properties error", e);
            return null;
        }
    }
}

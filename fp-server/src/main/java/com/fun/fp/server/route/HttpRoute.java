package com.fun.fp.server.route;


import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class HttpRoute implements java.io.Serializable {

    private int proxyPort;
    Map<String, List<Upstream>> upstreams;

    @Data
    public static class Upstream implements java.io.Serializable {
        private String host;
        private int port;
        private String path;
        private String domainName;

        private String prefix;
    }
}

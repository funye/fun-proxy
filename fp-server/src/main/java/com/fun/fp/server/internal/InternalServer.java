package com.fun.fp.server.internal;

import com.fun.fp.common.codec.InternalDecoder;
import com.fun.fp.common.codec.InternalEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

public class InternalServer extends Thread {

    private final int port;

    public InternalServer(int port) {
        this.port = port;
    }

    public void run() {

        EventLoopGroup boosGroup = new NioEventLoopGroup(4);
        EventLoopGroup workerGroup = new NioEventLoopGroup(16);

        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(boosGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
//                    .option(ChannelOption.SO_BACKLOG, 128) // 未连接和已连接队列大小
                    .localAddress(new InetSocketAddress(port))
                    .childHandler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast("encode", new InternalEncoder()); // 添加内部转发编码器
                            pipeline.addLast("decode", new InternalDecoder()); // 添加内部转发解码器
                            pipeline.addLast("internalServerHandler", new InternalServerHandler()); // 内部转发处理器
                        }
                    });

            ChannelFuture f = b.bind().sync();
            f.channel().closeFuture().sync();

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                workerGroup.shutdownGracefully().sync();
                boosGroup.shutdownGracefully().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.fun.fp.server.route;

import cn.hutool.json.JSONUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class HttpRouteConfig {

    private static Map<Integer, HttpRoute> routeMap;

    static {
        URL url = HttpRoute.class.getResource("/route.json");
        StringBuffer sb = new StringBuffer();
        String line = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(url.getFile()));
            line = in.readLine();
            while (line != null) {
                sb.append(line);
                line = in.readLine();
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<HttpRoute> routes = JSONUtil.toList(sb.toString(), HttpRoute.class);
        routeMap = routes.stream().collect(Collectors.toMap(HttpRoute::getProxyPort, a -> a, (k1, k2) -> k2));
    }

    public static List<HttpRoute> allRoute() {
        List<HttpRoute> list = new ArrayList<>();
        routeMap.forEach((k,v)->list.add(v));
        return list;
    }
    public static HttpRoute.Upstream route(int remotePort, String uri) {

        HttpRoute route = routeMap.get(remotePort);
        if (route == null) {
            return null;
        }
        List<HttpRoute.Upstream> targetServers = new ArrayList<>();
        route.getUpstreams().forEach((path, v) -> {
            if (uri.startsWith(path)) {
                v.forEach(e->e.setPrefix(path));
                targetServers.addAll(v);
            }
        });

        // 负载均衡
        HttpRoute.Upstream upstream = targetServers.get(new Random().nextInt(targetServers.size()));
        return upstream;
    }

    public static void main(String[] args) {
        System.out.println("------start------");
        System.out.println(route(8080, "/test"));
    }
}

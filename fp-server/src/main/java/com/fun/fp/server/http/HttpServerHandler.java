package com.fun.fp.server.http;

import com.fun.fp.common.message.InternalMessage;
import com.fun.fp.common.message.MessageTypeEnum;
import com.fun.fp.common.utils.HttpUtils;
import com.fun.fp.server.internal.Connection;
import com.fun.fp.server.internal.InternalChannelHolder;
import com.fun.fp.server.route.HttpRoute;
import com.fun.fp.server.route.HttpRouteConfig;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

import static io.netty.handler.codec.http.HttpUtil.is100ContinueExpected;

@Slf4j
public class HttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private int proxyPort;

    public HttpServerHandler(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
        InetSocketAddress ipSocket = (InetSocketAddress)ctx.channel().remoteAddress();
        String ip = ipSocket.getAddress().getHostAddress();
        int port = ipSocket.getPort();
        // 保存请求连接
        Connection conn = Connection.builder()
                .ctx(ctx)
                .host(ip)
                .port(port)
                .domainName(ipSocket.getHostName())
                .build();
        HttpChannelHolder.add(conn);

        //处理100 continue请求
        if (is100ContinueExpected(req)) {
            ctx.write(new DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.CONTINUE));
        }

        HttpRoute.Upstream upstream = HttpRouteConfig.route(proxyPort, req.uri());

        // 构造内部消息体，
        InternalMessage message = new InternalMessage();
        message.setMessageType(MessageTypeEnum.HTTP_REQUEST.getType());
        req.setUri(req.uri().replaceFirst(upstream.getPrefix(), upstream.getPath()));
        message.setData(HttpUtils.buildInternalHttpReq(req, ip, port));

        // 查询被代理的服务端, 并转发消息
        Connection httpServerConn = InternalChannelHolder.getHttpServer(upstream);
        if (httpServerConn != null && httpServerConn.getCtx().channel().isActive()) {
            httpServerConn.getCtx().writeAndFlush(message);
        } else {
            log.error("channel is not active......");
            InternalChannelHolder.removeHttpServer(httpServerConn);
        }

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}

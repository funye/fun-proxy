package com.fun.fp.client.internal;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.fun.fp.client.http.*;
import com.fun.fp.common.message.InternalMessage;
import com.fun.fp.common.message.MessageTypeEnum;
import com.fun.fp.common.message.http.HttpProxyInfo;
import com.fun.fp.common.message.http.HttpRegisterMessage;
import com.fun.fp.common.message.http.HttpRequestMessage;
import com.fun.fp.common.message.http.HttpResponseMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpResponse;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

@Slf4j
public class InternalClientHandler extends SimpleChannelInboundHandler<InternalMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, InternalMessage msg) throws Exception {
        MessageTypeEnum messageTypeEnum = MessageTypeEnum.of(msg.getMessageType());
        if (messageTypeEnum == null) {
            log.error("messageTypeEnum is null");
            return;
        }
        switch (messageTypeEnum) {
            case HTTP_REQUEST:
                HttpRequestMessage requestMessage = JSONUtil.toBean((JSONObject) msg.getData(), HttpRequestMessage.class);
                // 执行真实请求
                HttpClient.getInstance(HttpProxyConfig.getProxyInfo())
                        .sendRequest(requestMessage, new HttpCallback() {
                            @Override
                            public void onResponse(InternalMessage internalResp) {
                                // 回复消息给内部服务器
                                ctx.writeAndFlush(internalResp);
                            }

                            @Override
                            public void onError(InternalMessage internalResp, Throwable e) {
                                log.error("error");
                                ((HttpResponseMessage)internalResp.getData()).setBody("error".getBytes(StandardCharsets.UTF_8));
                                ctx.writeAndFlush(internalResp);
                            }
                        });
                break;
            default:
                break;
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        // 获取配置
        HttpProxyInfo info = HttpProxyConfig.getProxyInfo();

        // 发送注册消息
        HttpRegisterMessage registerMessage = new HttpRegisterMessage();
        registerMessage.setAppId(info.getUpstreamAppId());
        registerMessage.setAppHost(info.getUpstreamHost());
        registerMessage.setAppPort(info.getUpstreamPort());
        registerMessage.setDomainName(info.getUpstreamDomainName());

        InternalMessage internalResp = new InternalMessage();
        internalResp.setMessageType(MessageTypeEnum.HTTP_SERVER_REGISTER.getType());
        internalResp.setData(registerMessage);
        ctx.writeAndFlush(internalResp);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("close channel.....");
    }
}

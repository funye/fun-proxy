package com.fun.fp.client.http;

import com.fun.fp.common.message.InternalMessage;
import io.netty.channel.Channel;
import io.netty.channel.pool.ChannelPool;

public class HttpCallbackWrapper {

    private ChannelPool channelPool;
    private Channel channel;
    private HttpCallback callback;

    private String host;
    private int port;

    public HttpCallbackWrapper(ChannelPool channelPool, Channel channel, HttpCallback callback, String host, int port) {
        this.channelPool = channelPool;
        this.channel = channel;
        this.callback = callback;
        this.host = host;
        this.port = port;
    }

    public void onResponse(InternalMessage internalResp) {
        channelPool.release(channel);
        callback.onResponse(internalResp);
    }

    public void onError(InternalMessage internalResp, Throwable e) {
        channelPool.release(channel);
        callback.onError(internalResp, e);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}

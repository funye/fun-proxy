package com.fun.fp.client.http;


import com.fun.fp.client.config.SystemConfig;
import com.fun.fp.common.message.http.HttpProxyInfo;

public class HttpProxyConfig {

    public static HttpProxyInfo getProxyInfo() {
        HttpProxyInfo info = HttpProxyInfo.builder()
                .upstreamHost(SystemConfig.getProperty("upstream.host", ""))
                .upstreamPort(Integer.valueOf(SystemConfig.getProperty("upstream.port", "")))
                .upstreamAppId(SystemConfig.getProperty("upstream.appId", ""))
                .upstreamDomainName(SystemConfig.getProperty("upstream.domainName", ""))
                .build();
        return info;
    }

}

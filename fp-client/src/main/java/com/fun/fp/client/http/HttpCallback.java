package com.fun.fp.client.http;

import com.fun.fp.common.message.InternalMessage;

public interface HttpCallback {

    void onResponse(InternalMessage internalResp);

    void onError(InternalMessage internalResp, Throwable e);
}

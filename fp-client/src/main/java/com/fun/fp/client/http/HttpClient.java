package com.fun.fp.client.http;

import com.fun.fp.common.message.InternalMessage;
import com.fun.fp.common.message.MessageTypeEnum;
import com.fun.fp.common.message.http.HttpProxyInfo;
import com.fun.fp.common.message.http.HttpRequestMessage;
import com.fun.fp.common.message.http.HttpResponseMessage;
import com.fun.fp.common.utils.HttpUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.pool.ChannelHealthChecker;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;
import io.netty.channel.pool.FixedChannelPool;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpContentDecompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.Timer;
import io.netty.util.TimerTask;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class HttpClient {

    public final static Map<String, HttpCallbackWrapper> HTTP_CALLBACK_MAP = new ConcurrentHashMap<>(10000*50);
    private final static Map<String, HttpClient> HTTP_CLIENT_MAP = new ConcurrentHashMap<>();

    private long acquireConnectionTimeoutMills = 10000;
    private int maxConnections = 10;
    private int maxWaitConnections = 10;
    private long timeout=60*1000;

    private ChannelPool channelPool;

    private final static String RESPONSE_HANDLER = "RESPONSE";

    final Timer timer = new HashedWheelTimer(Executors.defaultThreadFactory(), 5, TimeUnit.SECONDS, 2);

    private HttpClient(final HttpProxyInfo proxyInfo) {

        // 构建bootstrap
        EventLoopGroup group = new NioEventLoopGroup(4);
        Bootstrap b = new Bootstrap();
        b.group(group)
                .option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 60000) // 超时时间?
                .channel(NioSocketChannel.class)
                .remoteAddress(new InetSocketAddress(proxyInfo.getUpstreamHost(), proxyInfo.getUpstreamPort()));

        // 构建连接池
        this.channelPool = new FixedChannelPool(b, new ChannelPoolHandler() {
            @Override
            public void channelReleased(Channel ch) throws Exception {

            }

            @Override
            public void channelAcquired(Channel ch) throws Exception {

            }

            @Override
            public void channelCreated(Channel ch) throws Exception {

                // ssl支持?

                // 设置处理器
                ch.pipeline()
                        .addLast(new HttpClientCodec())
                        .addLast(new HttpContentDecompressor())
                        .addLast(new HttpObjectAggregator(512 * 1024))
                        .addLast(RESPONSE_HANDLER, new HttpResponseHandler(ch));
            }
        }, ChannelHealthChecker.ACTIVE, FixedChannelPool.AcquireTimeoutAction.FAIL, acquireConnectionTimeoutMills, maxConnections, maxWaitConnections);

    }

    public static HttpClient getInstance(final HttpProxyInfo httpProxyInfo) {
        String key = httpProxyInfo.getUpstreamHost()+":"+httpProxyInfo.getUpstreamPort();
        if (HTTP_CLIENT_MAP.containsKey(key)) {
            return HTTP_CLIENT_MAP.get(key);
        } else {
            HttpClient client = new HttpClient(httpProxyInfo);
            HTTP_CLIENT_MAP.get(key);
            return client;
        }
    }

    public void sendRequest(HttpRequestMessage requestMessage, HttpCallback httpCallback) {
        channelPool.acquire().addListener(new GenericFutureListener<Future<? super Channel>>() {
            @Override
            public void operationComplete(Future<? super Channel> future) throws Exception {
                if (future.isSuccess()) {
                    Channel channel = null;
                    try {
                        channel = (Channel) future.get();
                        // 连接池获取连接成功，保存请求id与callback关系
                        String requestId = UUID.randomUUID().toString().replaceAll("-", "");

                        // 包装类，加入释放channel逻辑
                        HttpCallbackWrapper hcWrapper = new HttpCallbackWrapper(channelPool, channel, httpCallback, requestMessage.getIp(), requestMessage.getPort());
                        HttpClient.HTTP_CALLBACK_MAP.put(requestId, hcWrapper);

                        // 设置请求id
                        HttpResponseHandler responseHandler = (HttpResponseHandler) channel.pipeline().get(RESPONSE_HANDLER);
                        responseHandler.setRequestId(requestId);

                        // 执行请求
                        FullHttpRequest request = HttpUtils.buildHttpReq(requestMessage);
                        channel.writeAndFlush(request);

                        // 超时检查
                        timer.newTimeout(new TimeoutTask(channel, requestId), timeout, TimeUnit.MILLISECONDS);

                    } catch (Exception e) {
                        channelPool.release(channel);
                        InternalMessage internalResp = new InternalMessage();
                        httpCallback.onError(internalResp, e);
                    }
                } else {
                    // todo-yh: 异常情况处理
                    InternalMessage internalResp = new InternalMessage();
                    internalResp.setMessageType(MessageTypeEnum.HTTP_RESPONSE.getType());
                    HttpResponseMessage message = new HttpResponseMessage();
                    internalResp.setData(message);
                    httpCallback.onError(internalResp, future.cause());
                }

            }
        });
    }

    public static class TimeoutTask implements TimerTask {

        private Channel channel;
        private String requestId;

        public TimeoutTask(Channel channel, String requestId) {
            this.channel = channel;
            this.requestId = requestId;
        }

        @Override
        public void run(Timeout timeout) throws Exception {
            HttpCallbackWrapper callback = HttpClient.HTTP_CALLBACK_MAP.remove(requestId);
            if (callback != null) {
                callback.onError(null, new Exception("timeout")); // fixme: 返回值
            }
            channel.close();
        }
    }
}

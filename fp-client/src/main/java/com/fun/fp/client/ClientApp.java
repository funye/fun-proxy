package com.fun.fp.client;

import com.fun.fp.client.config.SystemConfig;
import com.fun.fp.client.internal.InternalClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientApp {

    private static final Logger log = LoggerFactory.getLogger(ClientApp.class);

    public static void main(String[] args) {
        System.setProperty("io.netty.tryReflectionSetAccessible", "true");
        try {
            String host = SystemConfig.getProperty("proxy.server.ip", "127.0.0.1");
            int port = Integer.valueOf(SystemConfig.getProperty("proxy.server.port", "9999"));
            new InternalClient(host,port).start();
            log.info("==========start ClientApp SUCCESS !!!=============");
        } catch (Exception e) {
            log.error("start ClientApp error", e);
        }
    }

}

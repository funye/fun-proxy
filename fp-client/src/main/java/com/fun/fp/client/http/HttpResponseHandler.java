package com.fun.fp.client.http;

import com.fun.fp.common.message.InternalMessage;
import com.fun.fp.common.message.MessageTypeEnum;
import com.fun.fp.common.utils.HttpUtils;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpResponse;
import lombok.Data;

@Data
public class HttpResponseHandler extends SimpleChannelInboundHandler<FullHttpResponse> {

    private String requestId;
    private Channel channel;

    public HttpResponseHandler(Channel channel) {
        this.channel = channel;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpResponse msg) throws Exception {
        // 处理结果
        HttpCallbackWrapper callback = HttpClient.HTTP_CALLBACK_MAP.remove(requestId);
        if (callback == null) {
            return;
        }
        InternalMessage internalResp = new InternalMessage();
        internalResp.setMessageType(MessageTypeEnum.HTTP_RESPONSE.getType());
        internalResp.setData(HttpUtils.buildInternalHttpResp(msg, callback.getHost(), callback.getPort()));

        // 回调，发送数据回服务端
        callback.onResponse(internalResp);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        channel.close(); // 连接异常，直接关闭

        HttpCallbackWrapper callback = HttpClient.HTTP_CALLBACK_MAP.remove(requestId);
        callback.onError(null, cause);// fixme: 返回值
        ctx.close();
    }
}

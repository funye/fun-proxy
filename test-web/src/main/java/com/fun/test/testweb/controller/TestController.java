package com.fun.test.testweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class TestController {

    @ResponseBody
    @RequestMapping("/test")
    public Object test(String name) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", "0000");
        result.put("msg","SUCCESS");
        result.put("data", name);
        return result;
    }

    @ResponseBody
    @RequestMapping("/test2")
    public Object test2(String name) {
        Map<String, Object> result = new HashMap<>();
        result.put("code", "0000");
        result.put("msg","SUCCESS");
        result.put("data", name);
        return result;
    }
}
